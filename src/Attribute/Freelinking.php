<?php

namespace Drupal\freelinking\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a freelinking plugin attribute.
 *
 * Plugin namespace: Plugin\freelinking.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Freelinking extends Plugin {

  /**
   * Initialization method.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   *   The human-readable name of the freelinking plugin. This is used as an
   *   administrative summary in the freelinking settings.
   * @param mixed[] $settings
   *   (optional) The freelinking plugin configuration.
   * @param int $weight
   *   (optional) The freelinking plugin weight. This is currently unused, and
   *   was not documented.
   * @param bool $hidden
   *   (optional) Describes whether or not the plugin is always enabled.
   * @param string|null $deriver
   *   (optional) The deriver class.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $title,
    public readonly array $settings = [],
    public readonly int $weight = 0,
    public readonly bool $hidden = FALSE,
    public readonly ?string $deriver = NULL,
  ) {}

}
