<?php

namespace Drupal\freelinking\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a freelinking plugin annotation object.
 *
 * Plugin namespace: Plugin\freelinking.
 *
 * @Annotation
 *
 * @deprecated in freelinking:4.0.2 and is removed from freelinking:5.0.0.
 *   plugins should use the attribute of the same name instead.
 *
 * @see https://www.drupal.org/node/3493351
 */
class Freelinking extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin provider.
   *
   * @var string
   */
  public $provider;

  /**
   * The human-readable name of the freelinking plugin.
   *
   * This is used as an administrative summary in the freelinking filter
   * settings.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  public $settings = [];

  /**
   * Describes whether or not the plugin can be disabled.
   *
   * @var bool
   */
  public $hidden = FALSE;

}
