<?php

namespace Drupal\Tests\freelinking\Unit\Plugin\freelinking;

use Drupal\Core\Language\Language;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Provides helper methods for freelinking node plugins.
 */
abstract class NodeTestBase extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Get a language object to pass into the plugin.
   *
   * @return \Drupal\Core\Language\LanguageInterface
   *   Return language object with default values.
   */
  public static function getDefaultLanguage() {
    return new Language(Language::$defaultValues);
  }

}
