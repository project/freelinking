<?php

namespace Drupal\Tests\freelinking\Functional;

/**
 * Tests freelinking tips on the Compose tips page.
 *
 * @group freelinking
 */
class FreelinkingFilterTipsTest extends FreelinkingBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Make sure that freelinking filter is activated.
    $this->updateFilterSettings();
  }

  /**
   * Tests the freelinking long tips.
   *
   * @covers \Drupal\freelinking\Plugin\Filter\Freelinking::tips().
   */
  public function testLongTips(): void {
    $this->drupalGet('/filter/tips');

    $this
      ->assertSession()
      ->pageTextContains('Path Alias [/path|alias/i]: Click to view a local node.');

    $this
      ->assertSession()
      ->pageTextContains('Search [/^search$/]: Search this site for content.');

    $this
      ->assertSession()
      ->pageTextContains('Wiki [/w((ikipedia|p)|(ikiquote|q)|(iktionary|t)|(ikinews|n)|(ikisource|s)|(ikibooks|b))?$/A]: Click to view a wiki page.');

    $this
      ->assertSession()
      ->pageTextContains('Google Search [/^google$/]: Search google for the specified terms.');

    $this
      ->assertSession()
      ->pageTextContains('Drupal.org External Link [/^d(rupal)?(o(rg)?|p(roject)?)$/]: Click to view on drupal.org.');

    $this
      ->assertSession()
      ->pageTextContains('File [/^file$/i]: Click to view a local file.');

    $this
      ->assertSession()
      ->pageTextContains('User [/(u|user|username|uid|userid)$/A]: Click to view user profile.');

    $this
      ->assertSession()
      ->pageTextContains('Node ID [/(n(id|ode)?)$/A]: Click to view a local node');

    $this
      ->assertSession()
      ->pageTextContains('Built-in [/^(showtext|nowiki|redact)$/]: Redact, show text only, or display the indicator');

    $this
      ->assertSession()
      ->pageTextContains('Node title [/nt$|nodetitle|title/A]: Click to view a local node.');

    $this
      ->assertSession()
      ->pageTextContains('External links [/^http(s)?|ext(ernal)?/]: Click to visit an external URL.');
  }

}
