<?php

/**
 * @file
 * Freelinking template preprocess functions.
 */

use Drupal\Core\Template\Attribute;

/**
 * Default implementation for template_preprocess_freelink().
 *
 * @param array &$variables
 *   The render element properties containing:
 *     - link: the link render array.
 *     - attributes: attributes to apply on the top-level element.
 */
function template_preprocess_freelink(array &$variables) {
  $variables['attributes'] = $variables['attributes'] ?? [];
  $variables['attributes'] = new Attribute($variables['attributes']);
}

/**
 * Default implementation for template_preprocess_freelink_error().
 *
 * @param array &$variables
 *   The render element properties containing:
 *     - plugin: the plugin ID that had an error.
 *     - message: the error message.
 *     - target: the search or lookup target.
 *     - attributes: attributes to apply on the top-level element.
 */
function template_preprocess_freelinking_error(array &$variables) {
  $variables['attributes']['classes'] += [
    'freelink-error', 'freelink-error--' . $variables['plugin'],
  ];

  $variables['attributes'] = new Attribute($variables['attributes']);
}
